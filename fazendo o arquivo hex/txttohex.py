with open('histogramGrayScaleHex.txt') as fp:
  for line in fp:
    array = line.split()
    #print array
    with open('histogramGrayScaleHex.hex', 'wb') as ds:
      count = 0
      offset = 0
      chksum = 0
      size = 16
      for i in array:
        if count % 16 == 0:
          chksum += size     
          hi = offset/255
          lo = offset&255
          chksum += hi
          chksum += lo      
          #print hex(offset), hex(hi), hex(lo), hi, chr(hi)
          ds.write((':'+format(size,'02x')+format(hi,'02x')+format(lo,'02x')+'00').upper())
        ds.write(format(int(i),'02x').upper())
        #print int(i), hex(int(i))
        chksum += int(i)
        count += 1
        #print count, i
        if count % 16 == 0: 
          #print "\n"
          #print hex(chksum)
          chksum = chksum&255
          #print hex(chksum)
          chksum = chksum ^ 255
          #print hex(chksum)
          chksum += 1
          #print hex(chksum)
          ds.write((format(chksum&255,'02x')+'\n').upper())
          chksum = 0
          offset += size
      ds.write(':00000001FF')
