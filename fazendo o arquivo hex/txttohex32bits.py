with open('histogramGrayScaleHex.hex', 'wb') as ds:
  count = 0
  offset = 0
  chksum = 0
  size = 8
  for i in range(4096+8):
    if count % size == 0:
      chksum += size     
      hi = offset/255
      lo = offset&255
      chksum += hi
      chksum += lo      
      #print hex(offset), hex(hi), hex(lo), hi, chr(hi)
      ds.write((':'+format(size,'02x')+format(hi,'02x')+format(lo,'02x')+'00').upper())
    ds.write(format(int(0),'02x').upper())
    #print int(i), hex(int(i))
    chksum += int(0)
    count += 1
    #print count, i
    if count % size == 0: 
      #print "\n"
      #print hex(chksum)
      chksum = chksum&255
      #print hex(chksum)
      chksum = chksum ^ 255
      #print hex(chksum)
      chksum += 1
      #print hex(chksum)
      ds.write((format(chksum&255,'02x')+'\n').upper())
      chksum = 0
      offset += size
  ds.write(':00000001FF')
