from intelhex import IntelHex
ih = IntelHex()
with open('histogramGrayScale.txt') as fp:
  for line in fp:
    array = line.split()
    for i in range(128*128):
      ih[i] = int(0)
      ih.write_hex_file("histogramGrayScale.hex")
