module otsuThreshold(input [63:0] threshLevel, input [7:0] in, input clk, output reg out);
	parameter SHIFT_AMOUNT = 16;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	reg [63:0] pixel;

	always @(posedge clk) begin
		pixel = in << SHIFT_AMOUNT;
		if (pixel >= threshLevel) begin
			out <= 1;
		end else begin
			out <= 0;
		end
	end
endmodule
