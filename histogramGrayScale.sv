module histogramGrayScale #(parameter GRAYSCALE_LEVELS = 255)(input [31:0] in, input clk, input start, output reg [31:0] out);
	reg [GRAYSCALE_LEVELS-1:0][31:0] histogram;
	shortint i1, i2, i3, i4, i;
	shortint o1, o2, count;
	
	always @(posedge clk) begin
		if (start) begin
			for (int i = 0; i < GRAYSCALE_LEVELS; i++) begin
				histogram[i] <= 32'b0;
			end
			count <= 16'b0;
			i <= 16'b0;
			
		end else begin
			if (count < 10) begin
				count <= count + 16'd1;
				
				i1 = 16'(in >> 24);
				i2 = 16'((in >> 16) & 32'hFF);
				i3 = 16'((in >> 8) & 32'hFF);
				i4 = 16'(in & 32'hFF);
			
				histogram[i1]++;
				histogram[i2]++;
				histogram[i3]++;
				histogram[i4]++;
			end
			
			//4096 = (128x128)x8bits/32bits
			else begin
				if (i < GRAYSCALE_LEVELS - 1) begin
					o1 = 16'(histogram[i]);
					o2 = 16'(histogram[i + 1]);
					
					out = {o1, o2};
					
					i <= i + 8'd2;
				end
			end
		end
	end
endmodule
