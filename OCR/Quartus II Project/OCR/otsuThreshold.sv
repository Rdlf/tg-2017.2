module otsuThreshold (
	pixel_in,
	pixel_out
);

input [63:0] pixel_in;
output [63:0] pixel_out;

int GRAYSCALE_LEVELS;
longint weightedSum, weightedSumFP, i;

fixedPoint weightedSumCast(.in(weightedSum), .internal(0), .out(weightedSumFP));

initial begin
	GRAYSCALE_LEVELS = 255;
	weightedSum = 0;
	pixel_out = 0;
end

always begin
	for (i = 0; i < GRAYSCALE_LEVELS; i = i + 1) begin
		weightedSumFP = weightedSumFP + i;
	end
end

endmodule
