module fixedPoint(in, internal, out);
	input [63:0] in;
	input internal;
	output [63:0] out;
	
	int SHIFT_AMOUNT;
	int SHIFT_MASK;
	initial begin
		SHIFT_AMOUNT = 16;
		SHIFT_MASK = (1 << SHIFT_AMOUNT) - 1;
	end

	always @(in) begin
		if (internal == 1) begin
			out = in;
		end else begin
			out = in << SHIFT_AMOUNT;
		end
	end
endmodule

module fixedPointAdd (a, b, c);
	input [63:0] a, b;
	output [63:0] c;
	reg [63:0] c;

	always @(a or b) begin
		c = a + b;
	end
endmodule

module fixedPointDiv (a, b, c);
	input [63:0] a, b;
	output [63:0] c;
	reg [63:0] c;
	
	int SHIFT_AMOUNT;
	int SHIFT_MASK;
	initial begin
		SHIFT_AMOUNT = 16;
		SHIFT_MASK = (1 << SHIFT_AMOUNT) - 1;
	end

	always @(a or b) begin
		c = (a << SHIFT_AMOUNT) / b;
	end
endmodule

module fixedPointMul (a, b, c);
	input [63:0] a, b;
	output [63:0] c;
	reg [63:0] c;
	
	int SHIFT_AMOUNT;
	int SHIFT_MASK;
	initial begin
		SHIFT_AMOUNT = 16;
		SHIFT_MASK = (1 << SHIFT_AMOUNT) - 1;
	end

	always @(a or b) begin
		c = a * b;
		c = c + ((c & 1 << (SHIFT_AMOUNT - 1)) << 1);
		c >>= SHIFT_AMOUNT;
	end
endmodule
