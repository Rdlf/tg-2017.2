module histogramBins (
		input clk,
		input rst,
		input start,
		input int q_a,
		input int q_b,
		output bit [11:0] address_a,
		output bit [11:0] address_b,
		output bit [39:0] out,
		output bit toNormalizedHistogram
	);
	parameter SHIFT_AMOUNT = 27;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	bit signed [127:0] temp, aux, angle, rightRatio, leftRatio, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right, weight;
	bit signed [127:0][0:2][0:2] atan2, getGradientMagnitude;
	bit signed [39:0] histogram[7][7][9]; //histogram = 14mb
	shortint leftBinIndex, rightBinIndex, x1, y1, z1, x2, y2, z2, b1, mod_a, mod_b;
	bit signed [15:0][0:9] histogramMap;
	shortint idebug, jdebug, kdebug;
	byte count, io, jo, lo, mo, dx, dy;
  
//	integer data_file;
//	integer scan_file;

	initial begin
		atan2 = {{128'h6487ed4, 128'h1921fb54, 128'h12d97c7e}, {128'hc90fda9, 128'h0, 128'hc90fdaa}, {128'h12d97c7e, 128'h0, 128'h6487ed4}};
		getGradientMagnitude = {{128'hb504f33, 128'h8000000, 128'hb504f33}, {128'h8000000, 128'h0, 128'h8000000}, {128'hb504f33, 128'h8000000, 128'hb504f33}};
		histogram = '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}};
		histogramMap = {16'd10, 16'd30, 16'd50, 16'd70, 16'd90, 16'd110, 16'd130, 16'd150, 16'd170};
		idebug = 0;
		jdebug = 0;
		kdebug = 0;
		io = 0;
		jo = 0;
		lo = 0;
		mo = 0;
		count = 0;
		address_a = 0;
		address_b = 0;
		toNormalizedHistogram = 0;
		
//		data_file = $fopen("histogramBins.txt", "w");
//		if (data_file == 0) begin
//			$display("data_file handle was NULL");
//			$finish;
//		end
	end
	
	always @(posedge clk) begin
		if (rst) begin
			histogram = '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}};
			idebug = 0;
			jdebug = 0;
			kdebug = 0;
			io = 0;
			jo = 0;
			lo = 0;
			mo = 0;
			count = 0;
			address_a = 0;
			address_b = 0;
			toNormalizedHistogram = 0;
		end
		
		if (start) begin
			if (count == 0) begin
				if (lo == 0) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + mo) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'((lo * 128 + mo) % 4);
				end else if (lo == 128 - 1) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'(((lo - 1) * 128 + mo) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'(((lo - 1) * 128 + mo) % 4);
				end else begin
					address_a = 12'(((lo + 1) * 128 + mo) / 4);
					address_b = 12'(((lo - 1) * 128 + mo) / 4);
					mod_a = 16'(((lo + 1) * 128 + mo) % 4);
					mod_b = 16'(((lo - 1) * 128 + mo) % 4);
				end
				//$fwrite(data_file, "a=%d b=%d mod_a=%d mod_b=%d\n", address_a, address_b, mod_a, mod_b);
				//$fwrite(data_file, "count=%d\n", debug);
			end
			
			if (count == 3) begin
				if (lo == 0) begin
					dx = 0;
				end else begin
					dx = 8'(((q_a >> $unsigned(8 * (3 - mod_a))) & 8'hFF) - ((q_b >> $unsigned(8 * (3 - mod_b))) & 8'hFF));
				end
				//$fwrite(data_file, "count=%d\n", debug);
				//$fwrite(data_file, "i=%d j=%d q_a=%h q_b=%h\n", lo, mo, ((q_a >> (8 * (3 - mod_a))) & 8'hFF), ((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				//$fwrite(data_file, "x: a=%d b=%d dx=%d dy=%d\n", address_a, address_b, dx, dy);
				
				if (mo == 0) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + mo) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'((lo * 128 + mo) % 4);
				end else if (mo == 128 - 1) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + (mo - 1)) / 4);
					mod_a = 16'((lo * 128 + mo) % 4);
					mod_b = 16'((lo * 128 + (mo - 1)) % 4);
				end else begin
					address_a = 12'((lo * 128 + (mo + 1)) / 4);
					address_b = 12'((lo * 128 + (mo - 1)) / 4);
					mod_a = 16'((lo * 128 + (mo + 1)) % 4);
					mod_b = 16'((lo * 128 + (mo - 1)) % 4);
				end
			end
			
			if (count == 6) begin
				if (mo == 0) begin
					dy = 0;
				end else begin
					dy = 8'(((q_a >> $unsigned(8 * (3 - mod_a))) & 8'hFF) - ((q_b >> $unsigned(8 * (3 - mod_b))) & 8'hFF));
				end
				//$fwrite(data_file, "count=%d\n", debug);
				//$fwrite(data_file, "i=%d j=%d q_a=%h q_b=%h\n", lo, mo, ((q_a >> (8 * (3 - mod_a))) & 8'hFF), ((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				//$fwrite(data_file, "i=%d j=%d dx=%d dy=%d\n", lo, mo, dx, dy);
				
				if (io <= 128 - 18) begin
					if (jo <= 128 - 18) begin
						if (lo < io + 18 && lo < 128) begin
							if (mo < jo + 18 && mo < 128) begin
								// angle*180.0 / PI
								temp = atan2[dx + 1][dy + 1] * 127'h1ca5dc1af;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								angle = temp;
								//angle = atan2[dx + 1][dy + 1];
								//if (angle != 0) begin
									//$fwrite(data_file, "angle=%h dx=%d dy=%d\n", atan2[dx + 1][dy + 1], dx, dy);
									//$fwrite(data_file, "i=%d j=%d l=%d m=%d angle=%d\n", io, jo, lo, mo, angle);
									//$fwrite(data_file, "y: a=%d b=%d pixel_a=%d pixel_b=%d\n", address_a, address_b, q_a, q_b);
									//$fwrite(data_file, "angle=%h angle*180=%h\n",atan2[dx + 1][dy + 1], temp);
								//end
								
								temp = (angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
								temp = temp + (1 << (SHIFT_AMOUNT - 1));
								leftBinIndex = 16'(temp >>> SHIFT_AMOUNT);
								leftBinIndex--;
								rightBinIndex = 16'(leftBinIndex + 1);
								if (leftBinIndex == -1) begin
									leftBinIndex = 16'(9 - 1);
								end
								if (rightBinIndex == 9) begin
									rightBinIndex = 0;
								end
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftBinIndex=%d rightBinIndex=%d get=%h angle=%h\n", io, jo, lo, mo, leftBinIndex, rightBinIndex, atan2[dx + 1][dy + 1], angle);
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftBinIndex=%d rightBinIndex=%d\n", io, jo, lo, mo, leftBinIndex, rightBinIndex);

								temp = (histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle;
								//$fwrite(data_file, "temp1=%h\n", temp);
								if (temp < 0) begin
									temp *= -1;
								end
								//$fwrite(data_file, "temp2=%h\n", temp);
								temp = (temp << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
								rightRatio = temp;
								leftRatio = (1 << SHIFT_AMOUNT) - rightRatio;

								if (leftBinIndex == (9 - 1)) begin
									leftRatio = 128'h4000000;
									rightRatio = 128'h4000000;
								end
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftRatio=%h rightRatio=%h leftBinIndex=%d\n", io, jo, lo, mo, leftRatio, rightRatio, leftBinIndex);

								x1 = 16'(mo / 18);
								y1 = 16'(lo / 18);
								z1 = 16'(((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT);
								if (z1 == 9) z1--;

								x2 = 16'(x1 + 1);
								y2 = 16'(y1 + 1);
								z2 = 16'(z1 + 1);
								if (x2 == 128 / 18) x2--;
								if (y2 == 128 / 18) y2--;
								if (z2 == 9) z2--;
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d x1=%d y1=%d z1=%d x2=%d y2=%d z2=%d\n", io, jo, lo, mo, x1, y1, z1, x2, y2, z2);
								
								//row_hist = io / 18;
								//cols_hist = jo / 18;
								b1 = 18;
								
								temp = ((mo - x1) / b1) - 1;
								aux = ((lo - y1) / b1) - 1;
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y1_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y1_right = temp;
								
								temp = ((mo - x1) / b1) - 1;
								aux = ((lo - y1) / b1);
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y2_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end							
								w_x1y2_right = temp;
								
								temp = ((mo - x1) / b1);
								aux = ((lo - y1) / b1) - 1;
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y1_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y1_right = temp;
								
								temp = ((mo - x1) / b1);
								aux = ((lo - y1) / b1);
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y2_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y2_right = temp;
								
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d w_x1y1_left=%h w_x1y1_right=%h w_x1y2_left=%h w_x1y2_right=%h w_x2y1_left=%h w_x2y1_right=%h w_x2y2_left=%h w_x2y2_right=%h\n", io, jo, lo, mo, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right);
								
								weight = getGradientMagnitude[dx + 1][dy + 1];
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d dx=%d dy=%d weigtht=%h\n", io, jo, lo, mo, dx, dy, weight);
								
								temp = w_x1y1_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x1][leftBinIndex] += 40'(temp);
								temp = w_x1y1_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x1][rightBinIndex] += 40'(temp);
								temp = w_x1y2_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x1][leftBinIndex] += 40'(temp);
								temp = w_x1y2_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x1][rightBinIndex] += 40'(temp);
								temp = w_x2y1_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x2][leftBinIndex] += 40'(temp);
								temp = w_x2y1_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x2][rightBinIndex] += 40'(temp);
								temp = w_x2y2_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x2][leftBinIndex] += 40'(temp);
								temp = w_x2y2_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x2][rightBinIndex] += 40'(temp);
								
								mo++;
								count = 0;
							end
							if (mo >= jo + 18 || mo >= 128) begin
								lo++;
								mo = jo;
							end
						end
						if (lo >= io + 18 || lo >= 128) begin
							jo += 8'd18;
							lo = io;
							mo = jo;
						end
					end
					if (jo > 128 - 18) begin
						io += 8'd18;
						jo = 0;
						lo = io;
						mo = jo;
					end
				end else begin
					toNormalizedHistogram = 1;
				end
			end else begin
				count++;
			end
		end
		
		if (toNormalizedHistogram) begin
			if (idebug < 7) begin
				if (jdebug < 7) begin
					if (kdebug < 9) begin
						out = histogram[idebug][jdebug][kdebug];
//						$fwrite(data_file, "histogram[%d][%d][%d]=%h\n", idebug, jdebug, kdebug, histogram[idebug][jdebug][kdebug]);
						kdebug++;
					end else begin
						jdebug++;
						kdebug=0;
					end
				end else begin
					idebug++;
					jdebug=0;
					kdebug=0;
				end
			end
		end
	end

endmodule