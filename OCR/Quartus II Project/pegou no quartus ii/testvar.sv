module testvar (
		input clk,
		input start,
		output bit [40:0] out
	);
	
	bit[40:0] histogram[7*7*9];
	shortint io;
	
	initial begin
		for (io = 0; io < 7*7*9; io++) begin
			histogram[io] = io;
		end
		io = 0;
	end
	
	always @(posedge clk) begin
		if (start) begin
			if (io < 7*7*9) begin
				out = histogram[io];
				
				io++;
			end
		end
	end

endmodule
