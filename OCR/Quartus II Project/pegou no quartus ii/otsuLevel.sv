module otsuLevel (
		input clk, 
		input rst,
		input start, 
		input bit[31:0] in, 
		output bit [127:0] out, 
		output bit toOtsuThreshold
	);
	parameter GRAYSCALE_LEVELS = 255;
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	bit [GRAYSCALE_LEVELS:0][15:0] histogram; //1mb
	bit [31:0] i, j;
	bit [15:0] i1, i2;
	int wB, wF, totalPixels;
	bit signed [127:0] backgroundSum, meanBackground, meanForeground, betweenClassVariance, maxVariance, weightedSum, temp;
	
 	// integer data_file;
	// integer print_file;
	
	initial begin
		i = 32'b0;
		j = 32'b0;
		weightedSum = 128'b0;
		wB = 32'b0;
		totalPixels = 32'd16384;
		maxVariance = 128'b0;
		backgroundSum = 128'b0;
		toOtsuThreshold = 1'b0;
		
		///data_file = $fopen("fixedPoint.txt", "w");
		//if (data_file == 0) begin
		//	$display("data_file handle was NULL");
		//	$finish;
		//end
	end
	
	always @(posedge clk) begin
		if (rst) begin
			i = 32'b0;
			j = 32'b0;
			weightedSum = 128'b0;
			wB = 32'b0;
			totalPixels = 32'd16384;
			maxVariance = 128'b0;
			backgroundSum = 128'b0;
			toOtsuThreshold = 1'b0;
		end
		
		if (start) begin
			if (i < GRAYSCALE_LEVELS) begin
				i1 = 16'(in >> 16);
				i2 = 16'(in & 16'hFFFF);
				
				histogram[i] = i1;
				histogram[i + 1] = i2;

				//$fwrite(data_file, "i=%d histogram[i]=%d\n", i, histogram[i]);
				//$fwrite(data_file, "i=%d histogram[i]=%d\n", i+1, histogram[i+1]);
				
				//(i/255)*i1
				temp = (64'((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i1 << SHIFT_AMOUNT);
				temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
				temp >>>= SHIFT_AMOUNT;
				weightedSum = weightedSum + temp;
				//$fwrite(data_file, "i=%d i1=%d (i/255)*i1=%h weightedSum=%h\n", i, i1, temp, weightedSum);
				temp = ((((i + 1) << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i2 << SHIFT_AMOUNT);
				temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
				temp >>>= SHIFT_AMOUNT;
				weightedSum = weightedSum + temp;
				//$fwrite(data_file, "i=%d i2=%d (i/255)*i2=%h weightedSum=%h\n", i+1, i2, temp, weightedSum);
				
				i = i + 2;
			end else begin
				if (j < GRAYSCALE_LEVELS) begin
					wB = wB + histogram[j];
					if (wB != 32'b0) begin	 
						wF = totalPixels - wB;
						if (wF != 32'b0) begin
							//(i/255)*i1
							temp = (64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (histogram[j] << SHIFT_AMOUNT);
							temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
							temp >>>= SHIFT_AMOUNT;
							backgroundSum = backgroundSum + temp;
							//meanBackground = backgroundSum / wB;
							meanBackground = (backgroundSum << SHIFT_AMOUNT) / (wB << SHIFT_AMOUNT);
							//meanForeground = (weightedSum - backgroundSum) / wF;
							meanForeground = ((weightedSum - backgroundSum) << SHIFT_AMOUNT) / (wF << SHIFT_AMOUNT);
							//betweenClassVariance = (double)wB * (double)wF * (meanBackground - meanForeground) * (meanBackground - meanForeground);
							temp = (wB << SHIFT_AMOUNT) * (wF << SHIFT_AMOUNT);
							temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
							temp >>>= SHIFT_AMOUNT;
							temp = temp * (meanBackground - meanForeground);
							temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
							temp >>>= SHIFT_AMOUNT;
							temp = temp * (meanBackground - meanForeground);
							temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
							temp >>>= SHIFT_AMOUNT;
							betweenClassVariance = temp;
							
							if (betweenClassVariance > maxVariance) begin
								maxVariance = betweenClassVariance;
								out = 64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
							end
							
							//$fwrite(data_file, "wB=%d wF=%d backgroundSum=%h\nmeanBackground=%h meanForeground=%h\nbetweenClassVariance=%h threshold=%h\n", wB, wF, backgroundSum, meanBackground, meanForeground, betweenClassVariance, threshold);

							//if (j == GRAYSCALE_LEVELS) begin
							//	$fclose(data_file);			
							//end
						end
					end
					
					j++;
				end else begin
					toOtsuThreshold = 1;			
				end
			end
		end 
	end
endmodule
