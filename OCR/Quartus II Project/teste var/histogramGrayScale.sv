module histogramGrayScale (
		input clk,
		input rst,
		input start, 
		input [31:0] in,
		output bit [31:0] out, 
		output bit toOtsuLevel
	);
	parameter GRAYSCALE_LEVELS = 255;

	//é preciso ter um a mais para enviar em pares
	bit [GRAYSCALE_LEVELS:0][15:0] histogram; //256*16 = 4096/4 = 1mb
	shortint i1, i2, i3, i4, i, o1, o2, count;
	
 	// integer data_file;
	// integer scan_file;

	initial begin
		for (shortint k = 0; k < GRAYSCALE_LEVELS + 1; k++) begin
			histogram[k] = 16'b0;
		end
		i = 16'b0;
		count = 16'b0;
		toOtsuLevel = 0;
		
 		// data_file = $fopen("histogramGrayScale.txt", "r");
		// if (data_file == 0) begin
			// $display("data_file handle was NULL");
			// $finish;
		// end
	end
	
	always @(posedge clk) begin
		if (rst) begin
			for (shortint k = 0; k < GRAYSCALE_LEVELS + 1; k++) begin
				histogram[k] = 16'b0;
			end
			i = 16'b0;
			count = 16'b0;
			toOtsuLevel = 0;
		end
		
		if (start) begin
			//4096 = (128x128)x8bits/32bits
			if (count < 4096) begin
				i1 = 16'(in >> 24);
				i2 = 16'((in >> 16) & 32'hFF);
				i3 = 16'((in >> 8) & 32'hFF);
				i4 = 16'(in & 32'hFF);
			
				histogram[i1]++;
				histogram[i2]++;
				histogram[i3]++;
				histogram[i4]++;

				count++;
			end else begin
				if (i < GRAYSCALE_LEVELS) begin
					o1 = histogram[i];
					o2 = histogram[i + 1];
						
					out = {o1, o2};
						
					i = i + 8'd2;
				end
				
				if (toOtsuLevel == 0) begin
					toOtsuLevel = 1;
				end
			end
		end
	end
endmodule
