module normalizedHistogram (
		input clk,
		input rst,
		input start,
		input [39:0] histogram_out,
		output bit [127:0] featuresVector_out,
		output bit end_signal
	);
	parameter SHIFT_AMOUNT = 27;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
  
//	integer data_file;
//	integer scan_file;
	
	byte io, jo, lo, mo, ko, step, e;
//	shortint featuresCounter;
	bit [39:0] histogram[7][7][9];
	bit [127:0] acumulator, temp, aux, sqrt_acumulator; //histogram = 14mb
	//bit [27:0] featuresVector[6*6*9*2*2]; //featuresVector = 9mb
	bit [7:0] out;
	bit [127:0] in;
	
	highbit highbit(.in(in), .out(out));
	
	initial begin
		io = 0;
		jo = 0;
		ko = 0;
		lo = 0;
		mo = 0;
		step = 0;
		//featuresCounter = 0;
		acumulator = 0;
	
//		data_file = $fopen("normalizedHistogram.txt", "w");
//		if (data_file == 0) begin
//			$display("data_file handle was NULL");
//			$finish;
//		end
	end
	
	always @(posedge clk) begin
		if (rst) begin
			io = 0;
			jo = 0;
			ko = 0;
			lo = 0;
			mo = 0;
			step = 0;
			//featuresCounter = 0;
			acumulator = 0;
		end
		
		if (start) begin
			case (step)
				0 : begin
					if (io < 7) begin
						if (jo < 7) begin
							if (ko < 9) begin
								histogram[io][jo][ko] = histogram_out;
//								$fwrite(data_file, "histogram[%d][%d][%d]=%h\n", io, jo, ko, histogram[io][jo][ko]);
							
								ko++;
							end else begin
								jo++;
								ko = 0;
							end
						end else begin
							io++;
							jo = 0;
						end
					end else begin
						step++;
						io = 0;
						jo = 0;
					end
				end
				
				1 : begin
					if (io < 6) begin
						if (jo < 6) begin
							if (lo < io + 2 && io < 7) begin
								if (mo < jo + 2 && jo < 7) begin
									if (ko < 9) begin
										//temp = histogram[lo][mo][ko] * histogram[lo][mo][ko];
										temp = histogram[lo][mo][ko];
										temp = temp * temp;
										temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
										temp >>>= SHIFT_AMOUNT;
										acumulator += temp;
										//$fwrite(data_file, "i=%d j=%d l=%d m=%d k=%d acumulator=%h\n", io, jo, lo, mo, ko, acumulator);
										
										ko++;
									end else begin
										mo++;
										ko = 0;
									end
								end else begin
									lo++;
									mo = jo;
									ko = 0;
								end
							end else begin
								//jo++;
								lo = io;
								mo = jo;
								ko = 0;			
								//acumulator = 128'h1400000;
								in = acumulator;		
								step++;
								//$fwrite(data_file, "acumulator=%h\n", acumulator);
							end
						end else begin
							io++;
							jo = 0;
							lo = io;
							mo = mo;
							ko = 0;
						end
					end else begin
						//featuresCounter = 0;
						//step = 4;
						//$fwrite(data_file, "end\n");
						if (end_signal == 0) begin
							end_signal = 1;
						end
					end
				end
				
				2 : begin
					e = out;
					
					if (e < 0) begin
						aux = acumulator << $unsigned(e * -1);
					end else begin
						aux = acumulator >> $unsigned(e);
					end
					e = 8'(e + 127);
					//$fwrite(data_file, "e=%h aux=%h\n", e, aux);
					temp = {1'b0,8'(e),23'(aux >> (SHIFT_AMOUNT - 23))};
					//$fwrite(data_file, "temp=%h\n", temp);
					
					temp = 128'h5F3759DF - (temp >> 1);
					//$fwrite(data_file, "result=%h\n", temp);   
					
					e = 8'((temp >> 23) - 127);
					//$fwrite(data_file, "e=%h\n", e); 
					aux = (1 << SHIFT_AMOUNT) + (23'(temp) << (SHIFT_AMOUNT - 23));
					if (e < 0) begin
						temp = aux >> $unsigned(e * -1);
					end else begin
						temp = aux << $unsigned(e);
					end
					//$fwrite(data_file, "aux=%h temp=%h\n", aux, temp); 
					
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					//y * y
					aux = temp * temp;
					aux = aux + ((aux & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux >>>= SHIFT_AMOUNT;
					//number * y * y
					aux = aux * acumulator;
					aux = aux + ((aux & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux >>>= SHIFT_AMOUNT;
					//number * 0.5 * y * y
					aux = aux * 128'h4000000;
					aux = aux + ((aux & 1 << (SHIFT_AMOUNT - 1)) << 1);
					aux >>>= SHIFT_AMOUNT;
					//threehalfs - ( x2 * y * y )
					aux = 128'hc000000 - aux;
					//y  = y * ( threehalfs - ( x2 * y * y ) )
					temp = temp * aux;
					temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp >>>= SHIFT_AMOUNT;
					
					temp = temp * acumulator;
					temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
					temp >>>= SHIFT_AMOUNT;
					sqrt_acumulator = temp;
					//$fwrite(data_file, "sqrt_acumulator=%h\n", sqrt_acumulator);
					step++;
				end	
				
				3 : begin
					if (lo < io + 2 && io < 7) begin
						if (mo < jo + 2 && jo < 7) begin
							if (ko < 9) begin
								//aux = (histogram[lo][mo][ko] << SHIFT_AMOUNT) / sqrt_acumulator;
								aux = histogram[lo][mo][ko];
								aux = (aux << SHIFT_AMOUNT) / sqrt_acumulator;
								//featuresVector[featuresCounter] = 28'(aux);
								if (aux > 128'h1999999) begin
									//featuresVector[featuresCounter] = 28'h1999999;
									aux = 28'h1999999;
								end
								
								//$fwrite(data_file, "i=%d j=%d k=%d histogram=%h featuresVector=%h\n", lo, mo, ko, histogram[lo][mo][ko], featuresVector[featuresCounter]);
								//$fwrite(data_file, "i=%d j=%d k=%d featuresVector=%h\n", lo, mo, ko, featuresVector[featuresCounter]);
//								$fwrite(data_file, "i=%d featuresVector_out=%h\n", featuresCounter, aux);
								//featuresCounter++;
								
								ko++;
							end else begin
								mo++;
								ko = 0;
							end
						end else begin
							lo++;
							mo = jo;
							ko = 0;
						end
					end else begin
						jo++;
						lo = io;
						mo = jo;
						ko = 0;
						acumulator = 0;
						step = 1;
					end
				end
				
				// 4 : begin
					// if (featuresCounter < 6*6*9*2*2) begin
						// featuresVector_out = 128'(featuresVector[featuresCounter]);
						// $fwrite(data_file, "i=%d featuresVector_out=%h\n", featuresCounter, featuresVector[featuresCounter]);
						
						// featuresCounter++;
					// end else begin
						// end_signal = 1;
					// end
				// end
			endcase
		end
	end
	
endmodule

module highbit #(
		parameter OUT_WIDTH = 8, // out uses one extra bit for not-found
		parameter IN_WIDTH = 1<<(OUT_WIDTH-1)
) (
		input [IN_WIDTH-1:0]in,
		output [OUT_WIDTH-1:0]out
);

wire [OUT_WIDTH-1:0]out_stage[0:IN_WIDTH];
assign out_stage[0] = ~8'b0; // desired default output if no bits set
genvar i;
generate
	for(i=0; i<IN_WIDTH; i=i+1) begin : high_bit_finder
		assign out_stage[i+1] = in[i] ? 8'(i - 27) : out_stage[i]; 
	end
endgenerate
assign out = out_stage[IN_WIDTH];

endmodule