module testBench (
		input clk,
		output bit [127:0] featuresVector_out
	);
//	integer data_file;
//	integer scan_file;
//	logic signed [31:0] captured_data;
	
	bit [11:0] address_a, address_b, address_a1, address_b1, address_a2, address_b2;
	int data_a, data_b, q_a, q_b;
	bit wren_a, wren_b;
	
	bit rst, start, toOtsuLevel, toOtsuThreshold, toHistogramBins, toNormalizedHistogram, end_signal;
	
	bit [31:0] pixels, histogram;
	bit [127:0] threshold;
	shortint count;
	
	bit [39:0] histogram_out;
	
	//ram
	ram2port32bits ram(.address_a(address_a), .address_b(address_b), .clock(clk), .data_a(data_a), .data_b(data_b), .wren_a(wren_a), .wren_b(wren_b), .q_a(q_a), .q_b(q_b));
	//otsu
	histogramGrayScale histogramGrayScale(.clk(clk), .rst(rst), .start(start), .in(pixels), .out(histogram), .toOtsuLevel(toOtsuLevel));
	otsuLevel otsuLevel(.clk(clk), .rst(rst), .start(toOtsuLevel), .in(histogram), .out(threshold), .toOtsuThreshold(toOtsuThreshold));
	otsuThreshold otsuThreshold(.clk(clk), .rst(rst), .start(start), .in(pixels), .threshold(threshold), .toOtsuThreshold(toOtsuThreshold), .q_a(q_a), .address_a(address_a1), .address_b(address_b1), .data_b(data_b), .wren_b(wren_b), .toHistogramBins(toHistogramBins));
	//hog
	histogramBins histogramBins(.clk(clk), .rst(rst), .start(toHistogramBins), .q_a(q_a), .q_b(q_b), .address_a(address_a2), .address_b(address_b2), .out(histogram_out), .toNormalizedHistogram(toNormalizedHistogram));
	normalizedHistogram normalizedHistogram(.clk(clk), .rst(rst), .start(toNormalizedHistogram), .histogram_out(histogram_out), .featuresVector_out(featuresVector_out), .end_signal(end_signal));
	
	initial begin
		count = 0;
		//temp = 0;
		start = 0;
		//flag = 0;
		
//		data_file = $fopen("histogramGrayScale.txt", "r");
//		if (data_file == 0) begin
//			$display("data_file handle was NULL");
//			$finish;
//		end
	end
	
	always @(posedge clk) begin
		//scan_file = $fscanf(data_file, "%d\r\n", captured_data);
		//temp = temp | captured_data << 8 * (3 - (count % 4));
		
		address_a = address_a1 | address_a2;
		address_b = address_b1 | address_b2;

		if (count <= 4096) begin
//			scan_file = $fscanf(data_file, "%h\r\n", captured_data);
//			pixels = captured_data;
			
			if (start == 0) begin
				start = 1;
			end

			// if (count % 4 == 3) begin
				// pixels = temp;
				// temp = 0;
				// if (start == 0) begin
					// start = 1;
				// end
			// end
		end 

		count++;

		// if (flag) begin
			// count++;
		// end
		// if (count == 16385) begin
			// count = 0;
			// flag = 1;
		// end
		
		if(end_signal) begin
			rst = 1;
			count = 0;
		end

//		if (!$feof(data_file)) begin
//			start = 0;
//		end
	end
endmodule
