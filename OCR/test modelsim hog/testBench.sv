module testBench (
		input clk,
		output bit [127:0] featuresVector_out
	);
	
	bit [13:0] address_a, address_b;
	byte q_a, q_b;
	bit toNormalizedHistogram;
	bit [127:0] histogram_out;
	
	ram2port ram(.address_a(address_a), .address_b(address_b), .clock(clk), .q_a(q_a), .q_b(q_b));
	histogramBins histogramBins(.clk(clk), .address_a(address_a), .address_b(address_b), .q_a(q_a), .q_b(q_b), .toNormalizedHistogram(toNormalizedHistogram), .histogram_out(histogram_out));
	normalizedHistogram normalizedHistogram(.clk(clk), .toNormalizedHistogram(toNormalizedHistogram), .histogram_out(histogram_out), .featuresVector_out(featuresVector_out));
	
endmodule