module otsuThreshold(input [127:0] threshold, input [31:0] in, input clk, input start, input thresholdReady, output reg out, output reg [8:0] address_a, output reg [8:0] address_b, output int data_a, output int data_b, output bit wren_a, output bit wren_b, input int q_a);
	parameter SHIFT_AMOUNT = 24;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	reg [127:0] pixel;
	//bit [16383:0][15:0] image;
	shortint count, i, j;
	byte image_a, image_b;
	
	integer data_file;
	integer print_file;
	
	initial begin
		count = 16'b0;
		i = 16'b0;
		j = 16'b0;
		address_a = 0;
		
		data_file = $fopen("otsuThreshold.txt", "w");
		if (data_file == 0) begin
			$display("data_file handle was NULL");
			$finish;
		end
	end

	always @(posedge clk) begin
		wren_a = 0;
		wren_b = 0;
		if (start) begin		
			if (count < 4096) begin
				data_a <= 16'(in >> 24);
				data_b <= 16'((in >> 16) & 32'hFF);
				image_a <= 16'((in >> 8) & 32'hFF);
				image_b <= 16'(in & 32'hFF);
				
				address_a = i;
				address_b = i + 1;
				
				wren_a = 1;
				wren_b = 1;
			end
		end else begin
			if (image_a != 0 & image_b != 0) begin
				address_a = i + 2;
				address_b = i + 3;
					
				wren_a = 1;
				wren_b = 1;
				
				data_a = image_a;
				data_b = image_b;
				image_a = 0;
				image_b = 0;
				
				count++;
				i = i + 4;
			end
		end
		if (thresholdReady) begin
			if (j < 16384 + 3) begin
				address_a = j;
			
				if (j > 2) begin
					pixel = ((q_a << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
					if (pixel >= threshold) begin
						out = 1;
					end else begin
						out = 0;
					end
					$fwrite(data_file, "j=%d out=%b\n", j-3, out);
				end
				
				//$fwrite(data_file, "j=%d pixel=%h out=%b\n", j, pixel, out);
				
				j++;
				//if (j == 16384) begin
					//$fclose(data_file);
				//end
			end
		end
	end
endmodule
