import re
with open('featuresVector_double.txt','wb') as cv:
  with open('featuresVector.txt') as fp:
    for line in fp:
      value =  re.search(r'[0-9a-f]{32}',line)
      if value:
        value = value.group()
        #print value
        value_int = int(value, 16)
        value_hex = hex(value_int)
        SHIFT_AMOUNT = 27
        SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1)
        #print value
        #print value_int
        #print value_hex
        #print hex(value_int >> SHIFT_AMOUNT)
        cv.write(re.sub(r'[0-9a-f]{32}',str((value_int >> SHIFT_AMOUNT) + (value_int & SHIFT_MASK) / float(1 << SHIFT_AMOUNT)), line))
        #break
      else:
        cv.write('end\n')
