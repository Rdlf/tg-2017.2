module histogramBins (
		input clk,
		output bit [11:0] address_a,
		output bit [11:0] address_b,
		input int q_a,
		input int q_b,
		input bit toHistogramBins,
		output bit toNormalizedHistogram,
		output bit [127:0] histogram_out,
		input shortint debug
	);
	parameter nBins = 9;
	parameter cellSize = 18;
	parameter SHIFT_AMOUNT = 27;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
  
	integer data_file;
	integer scan_file;

//	int divisions;
//	int firstAngle;
	bit signed [127:0] temp, aux, angle, atan2[3][3], rightRatio, leftRatio, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right, weight, getGradientMagnitude[3][3], histogram[7][7][9];
	shortint leftBinIndex, rightBinIndex, histogramMap[9], x1, y1, z1, x2, y2, z2, row_hist, cols_hist, b1, mod_a, mod_b;
	shortint idebug, jdebug, kdebug;
	byte count, io, jo, lo, mo, dx, dy;

	initial begin
		histogramMap <= '{16'd10, 16'd30, 16'd50, 16'd70, 16'd90, 16'd110, 16'd130, 16'd150, 16'd170};
//		divisions <= 32'd180;
//		firstAngle <= 32'b0;
		io <= 0;
		jo <= 0;
		lo <= 0;
		mo <= 0;
		atan2 <= '{'{128'h6487ed4, 128'h1921fb54, 128'h12d97c7e}, '{128'hc90fda9, 128'h0, 128'hc90fdaa}, '{128'h12d97c7e, 128'h0, 128'h6487ed4}};
		getGradientMagnitude <= '{'{128'hb504f33, 128'h8000000, 128'hb504f33}, '{128'h8000000, 128'h0, 128'h8000000}, '{128'hb504f33, 128'h8000000, 128'hb504f33}};
		histogram <= '{'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}},'{'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0},'{0,0,0,0,0,0,0,0,0}}};
		count <= 0;
		idebug <= 0;
		jdebug <= 0;
		kdebug <= 0;
		toNormalizedHistogram <= 0;
		
		data_file = $fopen("histogramBins.txt", "w");
		if (data_file == 0) begin
			$display("data_file handle was NULL");
			$finish;
		end
	end
	
	always @(posedge clk) begin
		if (toHistogramBins) begin
			if (count == 0) begin
				if (lo == 0) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + mo) / 4);
					mod_a = (lo * 128 + mo) % 4;
					mod_b = (lo * 128 + mo) % 4;
				end else if (lo == 128 - 1) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'(((lo - 1) * 128 + mo) / 4);
					mod_a = (lo * 128 + mo) % 4;
					mod_b = ((lo - 1) * 128 + mo) % 4;
				end else begin
					address_a = 12'(((lo + 1) * 128 + mo) / 4);
					address_b = 12'(((lo - 1) * 128 + mo) / 4);
					mod_a = ((lo + 1) * 128 + mo) % 4;
					mod_b = ((lo - 1) * 128 + mo) % 4;
				end
				//$fwrite(data_file, "a=%d b=%d mod_a=%d mod_b=%d\n", address_a, address_b, mod_a, mod_b);
				//$fwrite(data_file, "count=%d\n", debug);
			end
			
			if (count == 3) begin
				if (lo == 0) begin
					dx = 0;
				end else begin
					dx = ((q_a >> (8 * (3 - mod_a))) & 8'hFF) - ((q_b >> (8 * (3 - mod_b))) & 8'hFF);
				end
				//$fwrite(data_file, "count=%d\n", debug);
				//$fwrite(data_file, "i=%d j=%d q_a=%h q_b=%h\n", lo, mo, ((q_a >> (8 * (3 - mod_a))) & 8'hFF), ((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				//$fwrite(data_file, "x: a=%d b=%d dx=%d dy=%d\n", address_a, address_b, dx, dy);
				
				if (mo == 0) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + mo) / 4);
					mod_a = (lo * 128 + mo) % 4;
					mod_b = (lo * 128 + mo) % 4;
				end else if (mo == 128 - 1) begin
					address_a = 12'((lo * 128 + mo) / 4);
					address_b = 12'((lo * 128 + (mo - 1)) / 4);
					mod_a = (lo * 128 + mo) % 4;
					mod_b = (lo * 128 + (mo - 1)) % 4;
				end else begin
					address_a = 12'((lo * 128 + (mo + 1)) / 4);
					address_b = 12'((lo * 128 + (mo - 1)) / 4);
					mod_a = (lo * 128 + (mo + 1)) % 4;
					mod_b = (lo * 128 + (mo - 1)) % 4;
				end
			end
			
			if (count == 6) begin
				if (mo == 0) begin
					dy = 0;
				end else begin
					dy = ((q_a >> (8 * (3 - mod_a))) & 8'hFF) - ((q_b >> (8 * (3 - mod_b))) & 8'hFF);
				end
				//$fwrite(data_file, "count=%d\n", debug);
				//$fwrite(data_file, "i=%d j=%d q_a=%h q_b=%h\n", lo, mo, ((q_a >> (8 * (3 - mod_a))) & 8'hFF), ((q_b >> (8 * (3 - mod_b))) & 8'hFF));
				//$fwrite(data_file, "i=%d j=%d dx=%d dy=%d\n", lo, mo, dx, dy);
				
				if (io <= 128 - 18) begin
					if (jo <= 128 - 18) begin
						if (lo < io + cellSize && lo < 128) begin
							if (mo < jo + cellSize && mo < 128) begin
								// angle*180.0 / PI
								temp = atan2[dx + 1][dy + 1] * 127'h1ca5dc1af;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								angle = temp;
								//angle = atan2[dx + 1][dy + 1];
								//if (angle != 0) begin
									//$fwrite(data_file, "angle=%h dx=%d dy=%d\n", atan2[dx + 1][dy + 1], dx, dy);
									//$fwrite(data_file, "i=%d j=%d l=%d m=%d angle=%d\n", io, jo, lo, mo, angle);
									//$fwrite(data_file, "y: a=%d b=%d pixel_a=%d pixel_b=%d\n", address_a, address_b, q_a, q_b);
									//$fwrite(data_file, "angle=%h angle*180=%h\n",atan2[dx + 1][dy + 1], temp);
								//end
								
								temp = (angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
								temp = temp + (1 << (SHIFT_AMOUNT - 1));
								leftBinIndex = temp >>> SHIFT_AMOUNT;
								leftBinIndex--;
								rightBinIndex = leftBinIndex + 1;		
								if (leftBinIndex == -1) begin
									leftBinIndex = (9 - 1);
								end
								if (rightBinIndex == 9) begin
									rightBinIndex = 0;
								end
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftBinIndex=%d rightBinIndex=%d get=%h angle=%h\n", io, jo, lo, mo, leftBinIndex, rightBinIndex, atan2[dx + 1][dy + 1], angle);
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftBinIndex=%d rightBinIndex=%d\n", io, jo, lo, mo, leftBinIndex, rightBinIndex);

								temp = (histogramMap[leftBinIndex] << SHIFT_AMOUNT) - angle;
								//$fwrite(data_file, "temp1=%h\n", temp);
								if (temp < 0) begin
									temp *= -1;
								end
								//$fwrite(data_file, "temp2=%h\n", temp);
								temp = (temp << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT);
								rightRatio = temp;
								leftRatio = (1 << SHIFT_AMOUNT) - rightRatio;

								if (leftBinIndex == (9 - 1)) begin
									leftRatio = 128'h4000000;
									rightRatio = 128'h4000000;
								end
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d leftRatio=%h rightRatio=%h leftBinIndex=%d\n", io, jo, lo, mo, leftRatio, rightRatio, leftBinIndex);

								x1 = mo / 18;
								y1 = lo / 18;
								z1 = ((angle << SHIFT_AMOUNT) / (20 << SHIFT_AMOUNT)) >>> SHIFT_AMOUNT;
								if (z1 == nBins) z1--;

								x2 = x1 + 1;
								y2 = y1 + 1;
								z2 = z1 + 1;
								if (x2 == 128 / 18) x2--;
								if (y2 == 128 / 18) y2--;
								if (z2 == 9) z2--;
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d x1=%d y1=%d z1=%d x2=%d y2=%d z2=%d\n", io, jo, lo, mo, x1, y1, z1, x2, y2, z2);
								
								//row_hist = io / cellSize;
								//cols_hist = jo / cellSize;
								b1 = cellSize;
								
								temp = ((mo - x1) / b1) - 1;
								aux = ((lo - y1) / b1) - 1;
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y1_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y1_right = temp;
								
								temp = ((mo - x1) / b1) - 1;
								aux = ((lo - y1) / b1);
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x1y2_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end							
								w_x1y2_right = temp;
								
								temp = ((mo - x1) / b1);
								aux = ((lo - y1) / b1) - 1;
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y1_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y1_right = temp;
								
								temp = ((mo - x1) / b1);
								aux = ((lo - y1) / b1);
								aux = temp * aux;
								aux = aux << SHIFT_AMOUNT;
								
								temp = aux * leftRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y2_left = temp;
								
								temp = aux * rightRatio;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								if (temp < 0) begin
									temp *= -1;
								end
								w_x2y2_right = temp;
								
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d w_x1y1_left=%h w_x1y1_right=%h w_x1y2_left=%h w_x1y2_right=%h w_x2y1_left=%h w_x2y1_right=%h w_x2y2_left=%h w_x2y2_right=%h\n", io, jo, lo, mo, w_x1y1_left, w_x1y1_right, w_x1y2_left, w_x1y2_right, w_x2y1_left, w_x2y1_right, w_x2y2_left, w_x2y2_right);
								
								weight = getGradientMagnitude[dx + 1][dy + 1];
								//$fwrite(data_file, "i=%d j=%d l=%d m=%d dx=%d dy=%d weigtht=%h\n", io, jo, lo, mo, dx, dy, weight);
								
								temp = w_x1y1_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x1][leftBinIndex] += temp;
								temp = w_x1y1_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x1][rightBinIndex] += temp;
								temp = w_x1y2_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x1][leftBinIndex] += temp;
								temp = w_x1y2_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x1][rightBinIndex] += temp;
								temp = w_x2y1_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x2][leftBinIndex] += temp;
								temp = w_x2y1_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y1][x2][rightBinIndex] += temp;
								temp = w_x2y2_left * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x2][leftBinIndex] += temp;
								temp = w_x2y2_right * weight;
								temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
								temp >>>= SHIFT_AMOUNT;
								histogram[y2][x2][rightBinIndex] += temp;
								
								mo++;
								count = 0;
							end
							if (mo >= jo + cellSize || mo >= 128) begin
								lo++;
								mo = jo;
							end
						end
						if (lo >= io + cellSize || lo >= 128) begin
							jo += 8'(cellSize);
							lo = io;
							mo = jo;
						end
					end
					if (jo > 128 - 18) begin
						io += 8'(cellSize);
						jo = 0;
						lo = io;
						mo = jo;
					end
				end else begin
					toNormalizedHistogram = 1;
				end
			end else begin
				count++;
			end
		end
		
		if (toNormalizedHistogram) begin
			if (idebug < 7) begin
				if (jdebug < 7) begin
					if (kdebug < 9) begin
						histogram_out = histogram[idebug][jdebug][kdebug];
						$fwrite(data_file, "histogram[%d][%d][%d]=%h\n", idebug, jdebug, kdebug, histogram[idebug][jdebug][kdebug]);
						kdebug++;
					end else begin
						jdebug++;
						kdebug=0;
					end
				end else begin
					idebug++;
					jdebug=0;
					kdebug=0;
				end
			end
		end
	end

endmodule