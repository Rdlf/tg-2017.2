module testBench (input clk, output reg out);
	reg start, toOtsuLevel, thresholdReady;
	reg [31:0] in, temp, histogram;
	reg [127:0] threshold;
	shortint count;
	
	//integer data_file;
	//integer scan_file;
	//logic signed [31:0] captured_data;
	
	bit [13:0] address_a, address_b;
	byte q_a, q_b, step;
	
	ram2port ram(.address_a(address_a), .address_b(address_b), .clock(clk), .q_a(q_a), .q_b(q_b));
	histogramGrayScale histogramGrayScale(.in(in), .clk(clk), .start(start), .out(histogram), .toOtsuLevel(toOtsuLevel));
	otsuLevel otsuLevel(.in(histogram), .clk(clk), .start(toOtsuLevel), .threshold(threshold), .thresholdReady(thresholdReady));
	otsuThreshold otsuThreshold(.threshold(threshold), .in(in), .clk(clk), .start(start), .thresholdReady(thresholdReady), .out(out));
	
	initial begin
		count = 0;
		temp = 0;
		start = 0;
		step = 0;
		address_a = count;
		address_b = count + 1;
		
		// data_file = $fopen("histogramGrayScale.txt", "r");
		// if (data_file == 0) begin
			// $display("data_file handle was NULL");
			// $finish;
		// end
	end
	
	always @(posedge clk) begin
		// scan_file = $fscanf(data_file, "%d\r\n", captured_data);
	
		//start é uma flag para o histogramGrayScale
		if (start) begin
			start = 0;
		end
		
		//get 2 bytes saves in temp
		if (count >= 6) begin
			temp = temp | q_a << 8 * (3 - (address_a % 4));
			temp = temp | q_b << 8 * (3 - (address_b % 4));
		
			//when have 4 bytes in temp send to in
			if (count % 4 == 0) begin
				in = temp;
				temp = 0;
				if (start == 0) begin
					start = 1;
				end
			end
		end
		
		address_a = count;
		address_b = count + 1;
		
		//continue normally
		count += 2;
	end
endmodule
