
#include "debugar.h"


unsigned char pretaImg (cv::Mat src)
{
	int i,j;
	for (i = 0; i < src.rows ; i++)
	{
		for (j = 0; j < src.cols; j++)
		{
			src.at<cv::Vec3b>(i,j)[2]=255;
			src.at<cv::Vec3b>(i,j)[1]=255;
			src.at<cv::Vec3b>(i,j)[0]=255;
		}
	}

	return 'a';
}

/*Check if a Mat image (opencv) is equal to a RGBImg image*/
unsigned char areEqual (cv::Mat src, RGBImg * dst)
{
	int i,j;
	for (i = 0; i < src.rows ; i++)
	{
		for (j = 0; j < src.cols; j++)
		{

			if (dst->pixels[i][j].R != src.at<cv::Vec3b>(i,j)[2] ||
			dst->pixels[i][j].G != src.at<cv::Vec3b>(i,j)[1] ||
			dst->pixels[i][j].B != src.at<cv::Vec3b>(i,j)[0])
			{
				return 0;
			}
		}
	}
	return 1;

}


unsigned char Equalgray (cv::Mat src, GRAYImg * dst)
{
	int i,j;
	for (i = 0; i < src.rows ; i++)
	{
		for (j = 0; j < src.cols; j++)
		{
		//	printf("%d %d %d %d\n",i,j,dst->pixels[i][j],src.at<uchar>(i,j));
			if (dst->pixels[i][j] != src.at<uchar>(i,j))
			{
				return 0;
			}
		}
	}
	return 1;

}
