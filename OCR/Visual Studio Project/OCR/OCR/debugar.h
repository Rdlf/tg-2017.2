#ifndef DEBUGAR_INCLUDED
#define DEBUGAR_INCLUDED
#include "image.h"

/*Debug Functions*/
unsigned char pretaImg (cv::Mat src);
unsigned char areEqual (cv::Mat src, RGBImg * dst);
unsigned char Equalgray (cv::Mat src, GRAYImg * dst);


#endif // DEBUGAR_INCLUDED
