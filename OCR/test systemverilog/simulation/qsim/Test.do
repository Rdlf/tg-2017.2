onerror {exit -code 1}
vlib work
vlog -work work Test.vo
vlog -work work testBench.vwf.vt
vsim -novopt -c -t 1ps -L cycloneiv_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate_ver -L altera_lnsim_ver work.testBench_vlg_vec_tst -voptargs="+acc"
vcd file -direction Test.msim.vcd
vcd add -internal testBench_vlg_vec_tst/*
vcd add -internal testBench_vlg_vec_tst/i1/*
run -all
quit -f
