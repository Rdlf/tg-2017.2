module otsuThreshold(input [127:0] threshold, input [31:0] in, input clk, input start, input thresholdReady, output reg out);
	parameter SHIFT_AMOUNT = 32;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	reg [127:0] pixel;
	bit [15:0] image[16383:0];
	shortint count, i, j;
	
	integer data_file;
	integer print_file;
	
	initial begin
		count = 16'b0;
		i = 16'b0;
		j = 16'b0;
		
//		data_file = $fopen("otsuThreshold.txt", "w");
//		if (data_file == 0) begin
//			$display("data_file handle was NULL");
//			$finish;
//		end
	end

	always @(posedge clk) begin
		if (start) begin		
			if (count < 4096) begin
				image[i] <= 16'(in >> 24);
				image[i + 1] <= 16'((in >> 16) & 32'hFF);
				image[i + 2] <= 16'((in >> 8) & 32'hFF);
				image[i + 3] <= 16'(in & 32'hFF);
				
				count++;
				i = i + 16'd4;
			end
		end
		if (thresholdReady) begin
			if (j < 16384) begin
				pixel = ((image[j] << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
				if (pixel >= threshold) begin
					out = 1;
				end else begin
					out = 0;
				end
				
//				$fwrite(data_file, "j=%d pixel=%h out=%b\n", j, pixel, out);
				
				j++;
//				if (j == 16384) begin
//					$fclose(data_file);
//				end
			end
		end
	end
endmodule
