module counter (in, start, count, clk, overflow);
	input [3:0] in;
	input clk;
	input start;
	output reg [7:0] count;
	output reg overflow;
	
	always @ (posedge clk)
	  begin
		 if (start) begin
			count <= 8'b0;
			overflow <= 1'b0;
		 end

		 else if (in == 4'b0101) begin
			count <= count+1;
		 end
		 if (count == 4'b1111) begin
			overflow <=1'b1;
		 end
	  end
endmodule