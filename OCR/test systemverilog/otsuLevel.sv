module otsuLevel (input bit[31:0] in, input clk, input start, output reg [127:0] threshold, output reg thresholdReady);
	parameter GRAYSCALE_LEVELS = 255;
	parameter SHIFT_AMOUNT = 32;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	bit [GRAYSCALE_LEVELS:0][31:0] histogram;
	reg [31:0] i1, i2, i, j;
	int wB, wF, totalPixels;
	reg signed [127:0] backgroundSum, meanBackground, meanForeground, betweenClassVariance, maxVariance, weightedSum, temp;
	
	integer data_file;
	integer print_file;
	
	initial begin
		weightedSum <= 128'b0;
		i <= 32'b0;
		j <= 32'b0;
		wB <= 32'b0;
		wF <= 32'b0;
		totalPixels <= 32'd16384;
		maxVariance <= 128'b0;
		backgroundSum <= 128'b0;
		meanBackground <= 128'b0;
		thresholdReady <= 1'b0;
		
		///data_file = $fopen("fixedPoint.txt", "w");
		//if (data_file == 0) begin
		//	$display("data_file handle was NULL");
		//	$finish;
		//end
	end
	
	always @(posedge clk) begin
		if (start) begin
			if (i < GRAYSCALE_LEVELS) begin
				i1 = 32'(in >> 16);
				i2 = 32'(in & 32'hFFFF);
				
				histogram[i] = i1;
				histogram[i + 1] = i2;

				//$fwrite(data_file, "i=%d histogram[i]=%d\n", i, histogram[i]);
				//$fwrite(data_file, "i=%d histogram[i]=%d\n", i+1, histogram[i+1]);
				
				//(i/255)*i1
				temp = (64'((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i1 << SHIFT_AMOUNT);
				temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
				temp >>>= SHIFT_AMOUNT;
				weightedSum = weightedSum + temp;
				//$fwrite(data_file, "i=%d i1=%d (i/255)*i1=%h weightedSum=%h\n", i, i1, temp, weightedSum);
				temp = ((((i + 1) << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i2 << SHIFT_AMOUNT);
				temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
				temp >>>= SHIFT_AMOUNT;
				weightedSum = weightedSum + temp;
				//$fwrite(data_file, "i=%d i2=%d (i/255)*i2=%h weightedSum=%h\n", i+1, i2, temp, weightedSum);
				
				i = i + 2;
			end
		end else begin
			if (j < GRAYSCALE_LEVELS && i >= GRAYSCALE_LEVELS) begin
				wB = wB + histogram[j];
				if (wB != 32'b0) begin	 
					wF = totalPixels - wB;
					if (wF != 32'b0) begin
						//(i/255)*i1
						temp = (64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (histogram[j] << SHIFT_AMOUNT);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>>= SHIFT_AMOUNT;
						backgroundSum = backgroundSum + temp;
						//meanBackground = backgroundSum / wB;
						meanBackground = (backgroundSum << SHIFT_AMOUNT) / (wB << SHIFT_AMOUNT);
						//meanForeground = (weightedSum - backgroundSum) / wF;
						meanForeground = ((weightedSum - backgroundSum) << SHIFT_AMOUNT) / (wF << SHIFT_AMOUNT);
						//betweenClassVariance = (double)wB * (double)wF * (meanBackground - meanForeground) * (meanBackground - meanForeground);
						temp = (wB << SHIFT_AMOUNT) * (wF << SHIFT_AMOUNT);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>>= SHIFT_AMOUNT;
						temp = temp * (meanBackground - meanForeground);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>>= SHIFT_AMOUNT;
						temp = temp * (meanBackground - meanForeground);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>>= SHIFT_AMOUNT;
						betweenClassVariance = temp;
						
						if (betweenClassVariance > maxVariance) begin
							maxVariance = betweenClassVariance;
							threshold = 64'((j << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
						end
						
						//$fwrite(data_file, "wB=%d wF=%d backgroundSum=%h\nmeanBackground=%h meanForeground=%h\nbetweenClassVariance=%h threshold=%h\n", wB, wF, backgroundSum, meanBackground, meanForeground, betweenClassVariance, threshold);

						//if (j == GRAYSCALE_LEVELS) begin
						//	$fclose(data_file);			
						//end
					end
				end
				
				j++;
				if (j == GRAYSCALE_LEVELS) begin
					thresholdReady = 1;			
				end
			end
		end 
	end
endmodule
