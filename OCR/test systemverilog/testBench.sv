module testBench (input clk, output reg out);
	reg start, flag, toOtsuLevel, thresholdReady;
	logic signed [31:0] in, temp, histogram;
	reg [127:0] threshold;
	shortint count;
	
	integer data_file;
	integer scan_file;
	logic signed [31:0] captured_data;
	
	histogramGrayScale histogramGrayScale(.in(in), .clk(clk), .start(start), .out(histogram), .toOtsuLevel(toOtsuLevel));
	otsuLevel otsuLevel(.in(histogram), .clk(clk), .start(toOtsuLevel), .threshold(threshold), .thresholdReady(thresholdReady));
	otsuThreshold otsuThreshold(.threshold(threshold), .in(in), .clk(clk), .start(start), .thresholdReady(thresholdReady), .out(out));
	
	initial begin
		count = 0;
		temp = 0;
		start = 0;
		flag = 0;
		
//		data_file = $fopen("histogramGrayScale.txt", "r");
//		if (data_file == 0) begin
//			$display("data_file handle was NULL");
//			$finish;
//		end
	end
	
	always @(posedge clk) begin
//		scan_file = $fscanf(data_file, "%d\r\n", captured_data);
		temp = temp | captured_data << 8 * (3 - (count % 4));

		if (count <= 16384) begin
			if (start) begin
				start = 0;
			end

			if (count % 4 == 3) begin
				in = temp;
				temp = 0;
				if (start == 0) begin
					start = 1;
				end
			end
		end 

		count++;

		if (flag) begin
			count++;
		end
		if (count == 16385) begin
			count = 0;
			flag = 1;
		end

//		if (!$feof(data_file)) begin
//			start = 0;
//		end
	end
endmodule
