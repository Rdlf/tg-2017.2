#include "elm.hpp"

double getRandom(double min, double max) {
  /* Returns a random double between min and max */
  return (.8 - -.8) * ( (double)rand() / (double)RAND_MAX ) + -.8;
}

double getRandomInPower2(double min, double max) {
  /* Returns a random double between min and max */
  int power2 = (rand() % ((int)max - (int)min)) + (int)min;
  
  //(rand() % (int)(30))
  
  return pow(2,power2);
}

double sigmoid(double dataIn){
    return 1.0 / (1.0 + exp(-dataIn));
}

double hyperbolicTangent(double dataIn){
    return ((exp(dataIn)-exp(-dataIn))/(exp(dataIn)+exp(-dataIn)));
}

double reLU(double dataIn){
    if(dataIn > 0){
        return dataIn;
    }
    else{
        return 0;
    }
}

double activationFunction(double x, int kindOfFunction){
    //cout << x;
    double result;
    switch(kindOfFunction){
    case 0:
        result = x;
        break;
    case 1:
        result = sigmoid(x);
        break;
    case 2:
        result = hyperbolicTangent(x);
        break;
    default:
        result = x;
        break;
    }
    return result;
}

Elm::Elm(int inputNeuronsNumber, int hiddenNeuronsNumber, int outputNeuronsNumber, int numberOfPattern, double epsilon, double eta, unsigned int activationFunctionValue){

    unsigned int patt, feat, hidd;
    double tempRandomValue;
    double randomAux;
    this->inputNeuronsNumber = inputNeuronsNumber;
    this->hiddenNeuronsNumber = hiddenNeuronsNumber;
    this->outputNeuronsNumber = outputNeuronsNumber;
    this->patternNumber = numberOfPattern;
    
    this->initialWeigthMaxValue = 30;
    this->initialWeigthMinValue = -30;
    
    this->epsilon = epsilon;
    this->eta = eta;

    this->activationFunctionValue = activationFunctionValue;

    W.resize(this->inputNeuronsNumber, this->hiddenNeuronsNumber);
    
    for (feat=0; feat<this->inputNeuronsNumber; feat++){
        for (hidd=0; hidd<this->hiddenNeuronsNumber; hidd++){
            tempRandomValue = getRandom(-1.0, 1.0);     
            W(feat,hidd) = tempRandomValue;
        }   
    }
    B.resize(numberOfPattern, this->hiddenNeuronsNumber);
    
    
    for (hidd=0; hidd< this->hiddenNeuronsNumber; hidd++){
        randomAux = getRandom(-1.0, 1.0); 
        for (patt=0; patt< numberOfPattern; patt++){            
            B(patt,hidd) = randomAux;           
        }        
    }
    
    X.resize(numberOfPattern, this->inputNeuronsNumber);
    T.resize(numberOfPattern, this->outputNeuronsNumber);

}

void Elm::generateWeightBias(){

    unsigned int patt, feat, lab, hidd;
    double tempRandomValue;
    double randomAux;

    /*W_opencv.create(this->inputNeuronsNumber, this->hiddenNeuronsNumber, CV_64F);
    W_opencv = Scalar(0);//set to zeros */

    //W.resize(this->inputNeuronsNumber, this->hiddenNeuronsNumber);         
    
    for (feat=0; feat<this->inputNeuronsNumber; feat++){
        for (hidd=0; hidd<this->hiddenNeuronsNumber; hidd++){
            tempRandomValue = getRandom(-1.0, 1.0);            
            //W_opencv.at<double>(feat,hidd) = /*output_2 **/ (double)tempRandomValue;
            W(feat,hidd) = tempRandomValue;  
        }
    }
 
    /*B_opencv.create(this->patternNumber, this->hiddenNeuronsNumber, CV_64F);
    B_opencv = Scalar(0);//set to zeros*/
    
    //B.resize(this->patternNumber, this->hiddenNeuronsNumber);         
    
    for (hidd=0; hidd< this->hiddenNeuronsNumber; hidd++){
        randomAux = getRandom(-1.0, 1.0); /*(this->initialWeigthMinValue + (rand() % (int)(this->initialWeigthMaxValue - this->initialWeigthMinValue)));*/
        for (patt=0; patt< this->patternNumber; patt++){
            //B_opencv.at<double>(patt,hidd) = randomAux;
            B(patt,hidd) = randomAux;
        }
    }
}

void Elm::trainning(unsigned int setInitialWeigth, unsigned int setInitialBias, vector <vector <vector <double>*>*> *data){

    unsigned int i, j;
    unsigned int patt, feat, lab;

    
    for (patt=0; patt< data->size(); patt++){

        for (feat=0; feat<this->inputNeuronsNumber; feat++){

            X(patt,feat) = data->at(patt)->at(0)->at(feat);
            
        }
        
    }
    
    
    for (patt=0; patt<data->size(); patt++){
        for (lab=0; lab<this->outputNeuronsNumber;lab++){
            T(patt,lab)= data->at(patt)->at(1)->at(lab);
       }
    }        
    
    H_temp = X*W + B;
 //   printf("passei\n");

    for (i=0; i< H_temp.n_rows; i++){
		for (j=0; j< H_temp.n_cols; j++){            
		    H_temp(i,j) = activationFunction(H_temp(i,j),this->activationFunctionValue);                
		}
	}
	
	
    invH = pinv(H_temp);
 //   H_temp.resize(1,1);
    b = invH * T;
   // invH.resize(1,1);
    
}

int Elm::recognition(vector <double> *autoValorImagemAberta, int rejection){
    unsigned int i,j;
    int letraEscolhida=0;
    double nMin, secondNMin;
    
    X_aux.resize(1,this->inputNeuronsNumber);
    
    for (i=0; i<this->inputNeuronsNumber; i++){
    
        X_aux(0,i) = autoValorImagemAberta->at(i);
    
    
    }
    
    H_temp = X_aux*W;
  //  X_aux.resize(1,1);
    
    
    H_temp += B.row(0);
        
    
    for (i=0; i < H_temp.n_rows; i++){
		for (j=0; j < H_temp.n_cols; j++){
	
            H_temp(i,j) = activationFunction(H_temp(i,j),this->activationFunctionValue);
    
		}
    
	}
		
    T_temp = H_temp * b;
  //  H_temp.resize(1,1);
	
	
    letraEscolhida=0;
    
    nMin = T_temp(0,0);//, secondNMin;
    
    for (i=1; i< this->outputNeuronsNumber; i++){
    
        if (T_temp(0,i) > nMin){
            letraEscolhida = i;
            //nMin = T_temp_opencv.at<double>(0,i);
            nMin = T_temp(0,i);
        }
    }

    
    if(rejection == 0){
        return letraEscolhida;
    }
    else{
               


        if(letraEscolhida == 0) secondNMin = T_temp(0,1);
        else secondNMin = T_temp(0,0);
        
        for (i=1; i< this->outputNeuronsNumber; i++){

            if (T_temp(0,i) > secondNMin && i != letraEscolhida){
                //letraEscolhida = i;
                secondNMin = T_temp(0,i);
            }
        }
        
        if(abs(nMin - secondNMin) > this->epsilon){
            return letraEscolhida;
        }
        else{
            return -1; //rejection class
        }
    }
    

}

double Elm::recognitionRate(vector <vector <vector <double>*>*> *testingData){
    double totalHit = 0;
    double totalError = 0;
    int estimated = 0;
    for(unsigned int pattern = 0; pattern < testingData->size(); pattern++){
        
        estimated = this->recognition(testingData->at(pattern)->at(0),1);
        //cout << "estimated from elm: " << estimated << endl;
        if(testingData->at(pattern)->at(1)->at(estimated) == 1) totalHit++;
        else totalError++;
        //cout << "expected: ";
        /*cout << "[";
        for(unsigned int label = 0; label < testingData->at(pattern)->at(1)->size(); label++){
            cout << *testingData->at(pattern)->at(1)->at(label);
            if(label != testingData->at(pattern)->at(1)->size() - 1)
                cout << " ";
        }
        cout << "]";*/
        
    }
    
    //cout << "\ntotalHit: " << totalHit << endl;
    //cout << "totalError: " << totalError << endl;
    
    

    return totalHit/(totalHit+totalError);
}

void Elm::setHiddenNeuronsNumber(unsigned int hiddenNeuronsNumber){
    this->hiddenNeuronsNumber = hiddenNeuronsNumber;
    //coreObj = new Core(this->hiddenNeuronsNumber, this->outputNeuronsNumber);
}

double Elm::costFunction(vector <vector <vector <double>*>*> *testingData){
    //double totalHit = 0;
    //double totalError = 0;
    double cf = 0.0;
    double error = 0.0;
    int estimated = 0;
     double rejection = 0.0;
    //double cf = 0.0;
    //double error = 0.0;
   
    for(unsigned int pattern = 0; pattern < testingData->size(); pattern++){
        
        estimated = this->recognition(testingData->at(pattern)->at(0),1);
        
        //cout << "estimated: " << estimated << endl;
        //cout << "ops3" << endl;
        //getchar();
        
        //cout << "estimated: " << estimated << endl;
        //cout << "testingData->at(pattern)->at(1)->at(estimated): " << testingData->at(pattern)->at(1)->at(estimated) << endl;
        //cout << "estimated from elm: " << estimated << endl;
        if(estimated == -1){
            rejection++;
        }
        else{        
            if(testingData->at(pattern)->at(1)->at(estimated) == 0) error++;
            //else error++;
        }
        //cout << "expected: ";
        /*cout << "[";
        for(unsigned int label = 0; label < testingData->at(pattern)->at(1)->size(); label++){
            cout << *testingData->at(pattern)->at(1)->at(label);
            if(label != testingData->at(pattern)->at(1)->size() - 1)
                cout << " ";
        }
        cout << "]";*/
        
    }
    error = error/testingData->size();
    rejection = rejection/testingData->size();
    
    //cout << "testingData->size(): " << testingData->size() << endl;
    //getchar();

    //cout << "\ntotalHit: " << totalHit << endl;
    //cout << "totalError: " << totalError << endl;
    
    cf = eta * error + rejection;

    return cf;
}

double Elm::errorRate(vector <vector <vector <double>*>*> *testingData){
    //double totalHit = 0;
    //double totalError = 0;
    //double cf = 0.0;
    double error = 0.0;
    int estimated = 0;
     double rejection = 0.0;
    //double cf = 0.0;
    //double error = 0.0;
   
    for(unsigned int pattern = 0; pattern < testingData->size(); pattern++){
        
        estimated = this->recognition(testingData->at(pattern)->at(0),1);
        
        //cout << "estimated: " << estimated << endl;
        //cout << "ops3" << endl;
        //getchar();
        
        //cout << "estimated: " << estimated << endl;
        //cout << "testingData->at(pattern)->at(1)->at(estimated): " << testingData->at(pattern)->at(1)->at(estimated) << endl;
        //cout << "estimated from elm: " << estimated << endl;
        if(estimated == -1){
            rejection++;
        }
        else{        
            if(testingData->at(pattern)->at(1)->at(estimated) == 0) error++;
            //else error++;
        }
        //cout << "expected: ";
        /*cout << "[";
        for(unsigned int label = 0; label < testingData->at(pattern)->at(1)->size(); label++){
            cout << *testingData->at(pattern)->at(1)->at(label);
            if(label != testingData->at(pattern)->at(1)->size() - 1)
                cout << " ";
        }
        cout << "]";*/
        
    }
    error = error/testingData->size();
    rejection = rejection/testingData->size();
    
    //cout << "\ntotalHit: " << totalHit << endl;
    //cout << "totalError: " << totalError << endl;
    
    //cf = eta * error + rejection;

    return error;
}

double Elm::errorRateWithoutRejection(vector <vector <vector <double>*>*> *testingData){
    //double totalHit = 0;
    //double totalError = 0;
    //double cf = 0.0;
    double error = 0.0;
    int estimated = 0;
    //double rejection = 0.0;
    //double cf = 0.0;
    //double error = 0.0;
   
    for(unsigned int pattern = 0; pattern < testingData->size(); pattern++){
        
        estimated = this->recognition(testingData->at(pattern)->at(0),0);
        
        //cout << "estimated: " << estimated << endl;
        //cout << "ops3" << endl;
        //getchar();
        
        //cout << "estimated: " << estimated << endl;
        //cout << "testingData->at(pattern)->at(1)->at(estimated): " << testingData->at(pattern)->at(1)->at(estimated) << endl;
        //cout << "estimated from elm: " << estimated << endl;
        /*if(estimated == -1){
            rejection++;
        }
        else{*/        
        if(testingData->at(pattern)->at(1)->at(estimated) == 0) error++;
            //else error++;
        //}
        //cout << "expected: ";
        /*cout << "[";
        for(unsigned int label = 0; label < testingData->at(pattern)->at(1)->size(); label++){
            cout << *testingData->at(pattern)->at(1)->at(label);
            if(label != testingData->at(pattern)->at(1)->size() - 1)
                cout << " ";
        }
        cout << "]";*/
        
    }
    error = error/testingData->size();
    //rejection = rejection/testingData->size();
    
    //cout << "\ntotalHit: " << totalHit << endl;
    //cout << "totalError: " << totalError << endl;
    
    //cf = eta * error + rejection;

    return error;
}

double Elm::rejectionRate(vector <vector <vector <double>*>*> *testingData){
    //double totalHit = 0;
    //double totalError = 0;
    //double cf = 0.0;
    double error = 0.0;
    int estimated = 0;
     double rejection = 0.0;
    //double cf = 0.0;
    //double error = 0.0;
   
    for(unsigned int pattern = 0; pattern < testingData->size(); pattern++){
        
        estimated = this->recognition(testingData->at(pattern)->at(0),1);
        
        //cout << "estimated: " << estimated << endl;
        //cout << "ops3" << endl;
        //getchar();
        
        //cout << "estimated: " << estimated << endl;
        //cout << "testingData->at(pattern)->at(1)->at(estimated): " << testingData->at(pattern)->at(1)->at(estimated) << endl;
        //cout << "estimated from elm: " << estimated << endl;
        if(estimated == -1){
            rejection++;
        }
        else{        
            if(testingData->at(pattern)->at(1)->at(estimated) == 0) error++;
            //else error++;
        }
        //cout << "expected: ";
        /*cout << "[";
        for(unsigned int label = 0; label < testingData->at(pattern)->at(1)->size(); label++){
            cout << *testingData->at(pattern)->at(1)->at(label);
            if(label != testingData->at(pattern)->at(1)->size() - 1)
                cout << " ";
        }
        cout << "]";*/
        
    }
    error = error/testingData->size();
    rejection = rejection/testingData->size();
    
    //cout << "\ntotalHit: " << totalHit << endl;
    //cout << "totalError: " << totalError << endl;
    
    //cf = eta * error + rejection;

    return rejection;
}

double Elm::hitRate(vector <vector <vector <double>*>*> *testingData){
    //double totalHit = 0;
    //double totalError = 0;
    double hit = 0.0;
    //double cf = 0.0;
    double error = 0.0;
    int estimated = 0;
     double rejection = 0.0;
    //double cf = 0.0;
    //double error = 0.0;
   
    for(unsigned int pattern = 0; pattern < testingData->size(); pattern++){
        
        estimated = this->recognition(testingData->at(pattern)->at(0),1);
        
        //cout << "estimated: " << estimated << endl;
        //cout << "ops3" << endl;
        //getchar();
        
        //cout << "estimated: " << estimated << endl;
        //cout << "testingData->at(pattern)->at(1)->at(estimated): " << testingData->at(pattern)->at(1)->at(estimated) << endl;
        //cout << "estimated from elm: " << estimated << endl;
        if(estimated == -1){
            rejection++;
        }
        else{        
            if(testingData->at(pattern)->at(1)->at(estimated) == 0) error++;
            else hit++;
        }
        //cout << "expected: ";
        /*cout << "[";
        for(unsigned int label = 0; label < testingData->at(pattern)->at(1)->size(); label++){
            cout << *testingData->at(pattern)->at(1)->at(label);
            if(label != testingData->at(pattern)->at(1)->size() - 1)
                cout << " ";
        }
        cout << "]";*/
        
    }
    error = error/testingData->size();
    rejection = rejection/testingData->size();
    hit = hit/testingData->size();
    
    //cout << "\ntotalHit: " << totalHit << endl;
    //cout << "totalError: " << totalError << endl;
    
    //cf = eta * error + rejection;

    return hit;
}




      

