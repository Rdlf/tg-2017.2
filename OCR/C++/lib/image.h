#ifndef IMAGE_INCLUDED
#define IMAGE_INCLUDED

#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#define GRAYSCALE_LEVELS 255


/* RGB Structure */
typedef struct RGB
{
	unsigned char R;
	unsigned char G;
	unsigned char B;

} RGB;

/*RGB Images*/
typedef struct RGBImg
{
	unsigned int rows;
	unsigned int cols;
	RGB **pixels;
	
}RGBImg;

/* Binary/Grayscale Images */
typedef struct GRAYImg
{
	unsigned int rows;
	unsigned int cols;
	double **pixels;
	
}GRAYImg, BINImg;


typedef struct Histogram
{
	double* values;

} Histogram;



/*
*
*
	FUNCTIONS
*
*
*/	

void GRAYorBINImgToMat (GRAYImg *src);
void RGBImgToMat (cv::Mat dst, RGBImg *src);
void matToRGBImg (RGBImg *dst,cv::Mat src);


GRAYImg* createGRAYImg (unsigned int rows, unsigned int cols);
BINImg* createBINImg (unsigned int rows, unsigned int cols);
RGBImg* createRGBImg (unsigned int rows, unsigned int cols);





#endif // IMAGE_INCLUDED
