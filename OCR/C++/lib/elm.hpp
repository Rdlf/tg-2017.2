#ifndef ELM_H
#define ELM_H

#define  ARMA_DONT_USE_WRAPPER
#define  ARMA_USE_BLAS
#define  ARMA_USE_LAPACK

#include <iostream>
#include <armadillo>
//#include <bitset>         // std::bitset
//#include <armadillo>
//#include <boost/algorithm/string.hpp>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"

//#include "core.hpp"
//#include "dataset.hpp"
//#include "particle.hpp"
double getRandom(double min, double max);
double getRandomInPower2(double min, double max);
double reLU(double dataIn);
double hyperbolicTangent(double dataIn);
double sigmoid(double dataIn);
double activationFunction(double x, int kindOfFunction);

using namespace std;
//using namespace cv;
using namespace arma;

class Elm {
    private:
        
        double epsilon;
        double eta;
        unsigned int activationFunctionValue;
        
        double initialWeigthMaxValue, initialWeigthMinValue;
        
    public:
        Elm(int inputNeuronsNumber, int hiddenNeuronsNumber, int outputNeuronsNumber, int numberOfPattern, double epsilon, double eta, unsigned int activationFunctionValue);        
        void trainning(unsigned int setInitialWeigth, unsigned int setInitialBias, vector <vector <vector <double>*>*> *data);        
        int recognition(vector <double> *autoValorImagemAberta, int rejection);
        
        double errorRate(vector <vector <vector <double>*>*> *testingData);
        double errorRateWithoutRejection(vector <vector <vector <double>*>*> *testingData);
        double rejectionRate(vector <vector <vector <double>*>*> *testingData);
        double hitRate(vector <vector <vector <double>*>*> *testingData);          
        void setHiddenNeuronsNumber(unsigned int hiddenNeuronsNumber);
        double recognitionRate(vector <vector <vector <double>*>*> *testingData);
        double costFunction(vector <vector <vector <double>*>*> *testingData);  

        void generateWeightBias();    
	    mat A, invA, X, T, W, H, B, H_temp, invH, b, X_aux, T_temp;
        unsigned int inputNeuronsNumber, hiddenNeuronsNumber, outputNeuronsNumber, patternNumber;     
    
	     

};

#endif
