module histogramGrayScale (input [31:0] in, input clk, input start, output reg [31:0] out, output reg toOtsuLevel);
	parameter GRAYSCALE_LEVELS = 255;

	//é preciso ter um a mais para enviar em pares
	reg [GRAYSCALE_LEVELS:0][31:0] histogram;
	shortint i1, i2, i3, i4, i;
	shortint o1, o2, count;

	initial begin
		for (int i = 0; i < GRAYSCALE_LEVELS + 1; i++) begin
			histogram[i] = 32'b0;
		end
		count = 16'b0;
		i = 16'b0;
		toOtsuLevel = 0;
	end
	
	always @(posedge clk) begin
		if (start) begin
			if (count < 4096) begin
				i1 = 16'(in >> 24);
				i2 = 16'((in >> 16) & 32'hFF);
				i3 = 16'((in >> 8) & 32'hFF);
				i4 = 16'(in & 32'hFF);
			
				histogram[i1]++;
				histogram[i2]++;
				histogram[i3]++;
				histogram[i4]++;

				count++;
			end
		end
			
			//4096 = (128x128)x8bits/32bits
		if (count >= 4096) begin			
			if (toOtsuLevel == 0) begin
				toOtsuLevel = 1;
			end
			
			if (i < GRAYSCALE_LEVELS) begin
				o1 = 16'(histogram[i]);
				o2 = 16'(histogram[i + 1]);
					
				out = {o1, o2};
					
				i = i + 8'd2;
			end else begin
				if (toOtsuLevel) begin
					toOtsuLevel = 0;
				end				
			end
		end
	end
endmodule
