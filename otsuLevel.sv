module otsuLevel (input [31:0] in, input clk, input start, output reg [63:0] weightedSum);
	parameter GRAYSCALE_LEVELS = 255;
	parameter SHIFT_AMOUNT = 16;
	parameter SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
	
	reg [GRAYSCALE_LEVELS-1:0][31:0] histogram;
	shortint i1, i2, i;
	reg step;
	int wB, wF, totalPixels;
	longint backgroundSum, meanBackground, meanForeground, betweenClassVariance, maxVariance, threshold, temp;
	
	always @(posedge clk) begin
		if (start) begin
			for (int i = 0; i < GRAYSCALE_LEVELS; i++) begin
				histogram[i] <= 32'b0;
			end
			weightedSum <= 64'b0;
			i <= 16'b0;
			step <= 2'b0;
			wB <= 32'b0;
			wF <= 32'b0;
			totalPixels <= 32'd16384;
		end else begin
			case (step)
				1'd0 :
					if (i < GRAYSCALE_LEVELS - 1) begin
						i1 = 16'(in >> 16);
						i2 = 16'(in & 32'hFF);
						
						histogram[i] = i1;
						histogram[i + 1] = i2;
						
						//(i/255)*i1
						temp = (((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i1 << SHIFT_AMOUNT);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>= SHIFT_AMOUNT;
						weightedSum = weightedSum + temp;
						temp = (((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (i2 << SHIFT_AMOUNT);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>= SHIFT_AMOUNT;
						weightedSum = weightedSum + temp;

						i = i + 2;
					end else begin
						i <= 0;
						step <= 2'd1;
					end
				1'd1 :
					for (i = 0; i < GRAYSCALE_LEVELS; i++) begin
						wB = wB + histogram[i];
						if (wB == 32'b0) begin
							continue;
						end
						
						wF = totalPixels - wB;
						if (wF == 0) begin
							break;
						end
						
						//(i/255)*i1
						temp = (((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT)) * (histogram[i] << SHIFT_AMOUNT);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>= SHIFT_AMOUNT;
						backgroundSum = backgroundSum + temp;
						meanBackground = (backgroundSum << SHIFT_AMOUNT) / (wB << SHIFT_AMOUNT);
						meanForeground = (weightedSum - backgroundSum) / (wB << SHIFT_AMOUNT);
						temp = (wB << SHIFT_AMOUNT) * (wF << SHIFT_AMOUNT);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>= SHIFT_AMOUNT;
						temp = temp * (meanBackground - meanForeground);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>= SHIFT_AMOUNT;
						temp = temp * (meanBackground - meanForeground);
						temp = temp + ((temp & 1 << (SHIFT_AMOUNT - 1)) << 1);
						temp >>= SHIFT_AMOUNT;
						betweenClassVariance = temp;
						
						if (betweenClassVariance > maxVariance) begin
							maxVariance = betweenClassVariance;
							threshold = ((i << SHIFT_AMOUNT) << SHIFT_AMOUNT) / (255 << SHIFT_AMOUNT);
						end
					end
			endcase					
		end
	end
endmodule
