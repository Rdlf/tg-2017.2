while (1):
  value = raw_input("ponto fixo em hex: ")
  #value = "00000000000000000000000049494949"
  value_int = int(value, 16)
  value_hex = hex(value_int)
  SHIFT_AMOUNT = 27
  SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1)
  #print value
  #print value_int
  #print value_hex
  #print hex(value_int >> SHIFT_AMOUNT)
  print (value_int >> SHIFT_AMOUNT) + (value_int & SHIFT_MASK) / float(1 << SHIFT_AMOUNT)
