import codecs
hexlify = codecs.getencoder('hex')

while(1):
  value = input('pf to double: ')
  #value = '0x6487ed4'
  if value[0] == 'h':
    value = value[1:]
  SHIFT_AMOUNT = 27;
  SHIFT_MASK = ((1 << SHIFT_AMOUNT) - 1);
  value_int = int(value, 16)
  #print (value_int)
  #print hexlify(value_int & SHIFT_MASK)
  #print hexlify(value_int >> SHIFT_AMOUNT)
  print ((value_int >> SHIFT_AMOUNT) + ((value_int & SHIFT_MASK) / (1 << SHIFT_AMOUNT)))
